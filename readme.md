# plover-suffix-emoji

Stroke a suffix to a phrase to output an emoji.

## `=suffix_emoji`

Convert the last words inputted into an emoji.

Example:

```json
{
  "-PBLG": "=suffix_emoji"
}
```

* `ARPL/-PBLG` -> `💪`
* `ROEBT/-PBLG` -> `🤖`
* `ROEBT/ARPL/-PBLG` -> `🦾`
* `-PBLG` -> `-PBLG`

### Regional flags

Regional flags can be inputted by entering a 2-digit code (as standardized by
Unicode) and pressing the suffix stroke.

Example:

* `AQ/-PBLG` -> `🇦🇶`
* `UN/-PBLG -> `🇺🇳`
