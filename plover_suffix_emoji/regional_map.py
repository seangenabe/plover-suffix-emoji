from string import ascii_uppercase

# This is perfectly fine, these are just the regional indicators from A-Z.
regional_letters = "🇦🇧🇨🇩🇪🇫🇬🇭🇮🇯🇰🇱🇲🇳🇴🇵🇶🇷🇸🇹🇺🇻🇼🇽🇾🇿"

regional_map = dict(
  zip(ascii_uppercase, regional_letters),
)
