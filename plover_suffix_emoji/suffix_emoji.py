from string import ascii_uppercase
from plover.translation import Translator, Stroke, Translation
from plover_suffix_emoji.emoji_map import emoji_map
from plover_suffix_emoji.regional_map import regional_map
from plover.formatting import _Context, _Action, RetroFormatter
from pprint import pprint

ascii_uppercase_set = set(ascii_uppercase)
parsed_emoji_map = dict(
    (k, [v] if isinstance(v, str) else v) for k, v in emoji_map.items()
  )

def ensure_value(d, k, vf):
  if k in d:
    return d[k]
  v = vf(k)
  d[k] = v
  return v

def create_prefix_word_map():
  main_dict = {}
  for emoji, keyphrases in parsed_emoji_map.items():
    d = main_dict
    for keyphrase in keyphrases:
      for word in reversed(keyphrase.split(" ")):
        d = ensure_value(d, word, lambda k: dict())
      d[''] = emoji
  return main_dict

prefix_word_map = create_prefix_word_map()

def all_keyphrases():
  for v in parsed_emoji_map.values():
    yield from v

# Get max number of words in map
max_word_count = max(
  len(keyphrase.split(' ')) for keyphrase in all_keyphrases()
)

def assert_no_duplicates():
  s = set()
  for item in all_keyphrases():
    if item in s:
      assert False, f"{item} has been duplicated."
    s.add(item)

assert_no_duplicates()

# A prefix map of all words (ordered in reverse for each keyphrase)
prefix_word_map = create_prefix_word_map()

def suffix_emoji(translator: Translator, stroke: Stroke, arg: str) -> _Action:
  translations = translator.get_state().translations
  formatter = RetroFormatter(translations)

  # Chomp max_word_count words
  last_words = formatter.last_words(count = max_word_count)

  if len(last_words) == 0:
    translator.translate_translation(Translation([stroke], stroke.rtfcre))
    return

  # Look up last words
  emoji = None
  d = prefix_word_map
  # Replaced words in reverse order
  replaced_words = []
  for _word in reversed(last_words):
    word = _word.rstrip()
    if word in d:
      d = d[word]
      replaced_words.append(word)
      if "" in d:
        emoji = d[""]
    else:
      break

  p("emoji", emoji)
  p("last_words", last_words)
  p("replaced_words", replaced_words)

  if not emoji:
    last_word = last_words[-1].rstrip()
    if all(c in ascii_uppercase_set for c in last_word):
      t = Translation(
        [stroke],
        "".join(regional_map[c] for c in last_words[-1])
      )
      r = []
      for translation in reversed(translations):
        r.insert(0, translation)
        w = RetroFormatter(r).last_words(count = 1)[-1]
        if w == last_word:
          break

      t.replaced = r
      translator.translate_translation(t)
      return

    translator.translate_translation(Translation([stroke], stroke.rtfcre))
    return

  replaced_translations = []
  for t in reversed(translations):
    replaced_translations.insert(0, t)
    w = RetroFormatter(replaced_translations).last_words(
      count = len(replaced_words)
    )
    if replaced_words == w:
      break

  p("replaced_translations", replaced_translations)

  emoji_translation = Translation(
    [stroke],
    emoji
  )
  emoji_translation.replaced = replaced_translations

  translator.translate_translation(emoji_translation)

def p(s, obj):
  print(s, end=" ")
  pprint(obj)
